#include "../include/TTreeTable.h"

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
    // ����������� �������� ����� (� ���� ����������� �����)
    if (pNode != 0)
    {
        DeleteTreeTab(pNode->GetLeft());
        DeleteTreeTab(pNode->GetRight());
        delete pNode;
    }
}

int TTreeTable::IsFull() const
{
    // ������ �� ����� ���� �����������
    return false;
}

PTDatValue TTreeTable::FindRecord(TKey k)
{
    // ����� ������ (����� �� �������� �����)
    PTTreeNode temp = pRoot;
    ppRef = &pRoot; // ����� ��������� �� ������� ����������
    Efficiency = 0;
    while (temp != 0)
    {
        Efficiency++;
        if (temp->GetKey() == k)
            break;
        if (temp->GetKey() < k)
            ppRef = &temp->pRight;
        else
            ppRef = &temp->pLeft;
        temp = *ppRef;
    }
    if (temp == 0)
    {
        SetMessage(MSG_NO_RECORD);
        return 0;
    }
    else
    {
        SetMessage(MSG_OK);
        return temp->GetValuePTR();
    }
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
    if (FindRecord(k) != 0)
        SetMessage(MSG_TAB_REC_DBL);
    else
    {
        SetMessage(MSG_OK);
        *ppRef = new TTreeNode(k, pVal);
        DataCount++;
    }
}

void TTreeTable::DelRecord(TKey k)
{
    if (FindRecord(k) == 0)
        SetMessage(MSG_NO_RECORD);
    else
    {
        PTTreeNode tmp = pRoot;
        // ������� ���� ������������
        while (!St1.empty())
            St1.pop();
        // ����� �������� ����� �����
        while (tmp->GetKey() != k) {
            St1.push(tmp);
            if (tmp->GetKey() < k)
                tmp = tmp->GetRight();
            else
                tmp = tmp->GetLeft();
        }
        // �������� ����� ����� ���������� �����
        if ((tmp->pLeft == 0) && (tmp->pRight == 0))
        {
            // ���� ���� ��������
            if (!St1.empty())
            {
                // ����������� ������������ �����
                PTTreeNode prev = St1.top();
                if (prev != 0)
                {
                    // ������� ������ �� ������������ �����
                    if (prev->GetRight() == tmp)
                        prev->pRight = 0;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = 0;
                }
            }
            else
                pRoot = 0;
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ������ �������� (���������� �������� ��������, ������ ������ �������� ������ � �������� ��������� �� �������
        else if (tmp->pLeft == 0)
        {
            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                if (prev != 0)
                {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pRight;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pRight;
                }
            }
            else
                pRoot = tmp->GetRight();
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� �������� (���������� �������� � ������ ��������)
        else if (tmp->pRight == 0)
        {
            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                if (prev != 0)
                {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pLeft;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pLeft;
                }
            }
            else
                pRoot = tmp->GetLeft();
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� ���������
        else
        {
            // ������������� ����� �����, ������� ��������� �����, � ��������� �� �������� ����� ����� ���������
            // ��� ����� ������ ���� � ������� ����� (�������), ����� �������� ������ �� ������������ ��������, ���� ������������� �����
            PTTreeNode down_left = tmp->GetRight();
            while (down_left->GetLeft() != 0)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->GetLeft();

            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                // ������������� ������ ����� � ��������
                if (prev != 0)
                {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pRight;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pRight;
                }
            }
            else
                pRoot = tmp->GetRight();

            delete tmp;
            DataCount--;
        }

    }
}

TKey TTreeTable::GetKey(void)
{
    if (pCurrent == 0)
        return string("");
    else
        return pCurrent->Key;
}

PTDatValue TTreeTable::GetValuePTR(void)
{
    if (pCurrent == 0)
        return 0;
    else
        return pCurrent->pValue;
}

int TTreeTable::Reset(void) {
    PTTreeNode temp = pCurrent = pRoot;
    while (!St.empty())
        St.pop(); // ������� ����� ������������
    CurrPos = 0;
    while (temp != 0)
    {
        St.push(temp);
        pCurrent = temp;
        temp = temp->GetLeft();
    }
    SetMessage(MSG_OK);
    return 0;
}

int TTreeTable::IsTabEnded(void) const
{
    return (CurrPos >= DataCount);
}

int TTreeTable::GoNext(void)
{
    // ������������ �� ������������ �������� �� �������������
    // ���� �� ����� �����, ����� �������� ������� �������, ��� ��������� ����� ���� �������
    CurrPos++;
    if (!IsTabEnded() && (pCurrent != 0))
    {
        PTTreeNode temp = pCurrent = pCurrent->GetRight();
        St.pop();
        while (temp != 0)
        {
            St.push(temp);
            pCurrent = temp;
            temp = temp->GetLeft();
        }
        if ((pCurrent == 0) && !St.empty())
            pCurrent = St.top();
    }
    else
    {
        SetMessage(MSG_OUT_OF_RANGE);
        return GetMessage();
    }
}