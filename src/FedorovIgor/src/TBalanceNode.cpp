#include "../include/TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
    TBalanceNode *temp = new TBalanceNode(Key, pValue, 0, 0, BalOk);
    return temp;
}

int TBalanceNode::GetBalance(void) const
{
    return Balance;
}

void TBalanceNode::SetBalance(int bal)
{
    Balance = bal;
}